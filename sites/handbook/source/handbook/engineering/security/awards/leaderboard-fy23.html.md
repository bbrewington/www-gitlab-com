---
layout: handbook-page-toc
title: "Security Awards Leaderboard"
---

### On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page is [auto-generated and updated every Mondays](../security-awards-program.html#process).

# Leaderboard FY23

## Yearly

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 1600 |
| [@vitallium](https://gitlab.com/vitallium) | 2 | 900 |
| [@tkuah](https://gitlab.com/tkuah) | 3 | 700 |
| [@kassio](https://gitlab.com/kassio) | 4 | 600 |
| [@brodock](https://gitlab.com/brodock) | 5 | 600 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 6 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 7 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 8 | 600 |
| [@.luke](https://gitlab.com/.luke) | 9 | 500 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 10 | 500 |
| [@ratchade](https://gitlab.com/ratchade) | 11 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 12 | 500 |
| [@jlear](https://gitlab.com/jlear) | 13 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 14 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 15 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 16 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 17 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 18 | 460 |
| [@garyh](https://gitlab.com/garyh) | 19 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 20 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 21 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 22 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 23 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 24 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 25 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 26 | 150 |
| [@10io](https://gitlab.com/10io) | 27 | 100 |
| [@pshutsin](https://gitlab.com/pshutsin) | 28 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 29 | 100 |
| [@jerasmus](https://gitlab.com/jerasmus) | 30 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 31 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 32 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 33 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 34 | 60 |
| [@minac](https://gitlab.com/minac) | 35 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 36 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 37 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 38 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 39 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 40 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 41 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 42 | 30 |
| [@subashis](https://gitlab.com/subashis) | 43 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 44 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 45 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 46 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 47 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 48 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 49 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@f_santos](https://gitlab.com/f_santos) | 1 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 2 | 300 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 3 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 700 |
| [@spirosoik](https://gitlab.com/spirosoik) | 2 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 3 | 200 |
| [@zined](https://gitlab.com/zined) | 4 | 200 |

## FY23-Q1

### Development

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@leipert](https://gitlab.com/leipert) | 1 | 1600 |
| [@vitallium](https://gitlab.com/vitallium) | 2 | 900 |
| [@tkuah](https://gitlab.com/tkuah) | 3 | 700 |
| [@kassio](https://gitlab.com/kassio) | 4 | 600 |
| [@brodock](https://gitlab.com/brodock) | 5 | 600 |
| [@hfyngvason](https://gitlab.com/hfyngvason) | 6 | 600 |
| [@sethgitlab](https://gitlab.com/sethgitlab) | 7 | 600 |
| [@cam_swords](https://gitlab.com/cam_swords) | 8 | 600 |
| [@.luke](https://gitlab.com/.luke) | 9 | 500 |
| [@Andysoiron](https://gitlab.com/Andysoiron) | 10 | 500 |
| [@ratchade](https://gitlab.com/ratchade) | 11 | 500 |
| [@rossfuhrman](https://gitlab.com/rossfuhrman) | 12 | 500 |
| [@jlear](https://gitlab.com/jlear) | 13 | 500 |
| [@acunskis](https://gitlab.com/acunskis) | 14 | 500 |
| [@peterhegman](https://gitlab.com/peterhegman) | 15 | 500 |
| [@sgoldstein](https://gitlab.com/sgoldstein) | 16 | 500 |
| [@atiwari71](https://gitlab.com/atiwari71) | 17 | 500 |
| [@cwoolley-gitlab](https://gitlab.com/cwoolley-gitlab) | 18 | 460 |
| [@garyh](https://gitlab.com/garyh) | 19 | 440 |
| [@georgekoltsov](https://gitlab.com/georgekoltsov) | 20 | 400 |
| [@stanhu](https://gitlab.com/stanhu) | 21 | 400 |
| [@kerrizor](https://gitlab.com/kerrizor) | 22 | 400 |
| [@rcobb](https://gitlab.com/rcobb) | 23 | 300 |
| [@toupeira](https://gitlab.com/toupeira) | 24 | 240 |
| [@m_frankiewicz](https://gitlab.com/m_frankiewicz) | 25 | 200 |
| [@alexkalderimis](https://gitlab.com/alexkalderimis) | 26 | 150 |
| [@10io](https://gitlab.com/10io) | 27 | 100 |
| [@pshutsin](https://gitlab.com/pshutsin) | 28 | 100 |
| [@mattkasa](https://gitlab.com/mattkasa) | 29 | 100 |
| [@jerasmus](https://gitlab.com/jerasmus) | 30 | 80 |
| [@dmakovey](https://gitlab.com/dmakovey) | 31 | 80 |
| [@mwoolf](https://gitlab.com/mwoolf) | 32 | 60 |
| [@dskim_gitlab](https://gitlab.com/dskim_gitlab) | 33 | 60 |
| [@proglottis](https://gitlab.com/proglottis) | 34 | 60 |
| [@minac](https://gitlab.com/minac) | 35 | 60 |
| [@egrieff](https://gitlab.com/egrieff) | 36 | 60 |
| [@hortiz5](https://gitlab.com/hortiz5) | 37 | 60 |
| [@mbobin](https://gitlab.com/mbobin) | 38 | 50 |
| [@ghickey](https://gitlab.com/ghickey) | 39 | 40 |
| [@bdenkovych](https://gitlab.com/bdenkovych) | 40 | 40 |
| [@felipe_artur](https://gitlab.com/felipe_artur) | 41 | 40 |
| [@ohoral](https://gitlab.com/ohoral) | 42 | 30 |
| [@subashis](https://gitlab.com/subashis) | 43 | 30 |
| [@ebaque](https://gitlab.com/ebaque) | 44 | 30 |
| [@brytannia](https://gitlab.com/brytannia) | 45 | 30 |
| [@mkaeppler](https://gitlab.com/mkaeppler) | 46 | 30 |
| [@jprovaznik](https://gitlab.com/jprovaznik) | 47 | 20 |
| [@ntepluhina](https://gitlab.com/ntepluhina) | 48 | 20 |
| [@mikolaj_wawrzyniak](https://gitlab.com/mikolaj_wawrzyniak) | 49 | 20 |

### Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@f_santos](https://gitlab.com/f_santos) | 1 | 300 |
| [@skarbek](https://gitlab.com/skarbek) | 2 | 300 |
| [@katiemacoy](https://gitlab.com/katiemacoy) | 3 | 50 |
| [@rspeicher](https://gitlab.com/rspeicher) | 4 | 30 |

### Non-Engineering

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@vburton](https://gitlab.com/vburton) | 1 | 30 |

### Community

| Nominee | Rank | Points |
| ------- | ----:| ------:|
| [@rpadovani](https://gitlab.com/rpadovani) | 1 | 700 |
| [@spirosoik](https://gitlab.com/spirosoik) | 2 | 300 |
| [@benyanke](https://gitlab.com/benyanke) | 3 | 200 |
| [@zined](https://gitlab.com/zined) | 4 | 200 |


