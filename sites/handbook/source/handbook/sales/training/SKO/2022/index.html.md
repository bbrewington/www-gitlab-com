---
layout: handbook-page-toc
title: "Sales Kickoff 2022"
description: "GitLab Sales Kickoff 2022"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Overview
GitLab SKO 2022 will take place virtually from March 15-17, 2022. The virtual event will consist of three half-days offered in two time zone options to accommodate our team members around the world. To learn more about the event, see the [Field Team FAQ document](https://docs.google.com/document/d/1OTRQjtWs-8sAnZG_ITQwNn0B2bZ6ed6sqhDEWKlimqE/edit#). 

## SKO 2022 Event Promo Video

<figure class="video_container">
  <iframe src="https://player.vimeo.com/video/673286284?h=4d4bf9f431&color=fc6d26" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

# Coming Soon 
Links to videos (all GitLab internal-only) will be added below within 1-2 weeks following our live virtual Sales Kickoff event. Videos can also be found in the [Sales Kickoff 2022 channel in EdCast](https://gitlab.edcast.com/channel/sales-kickoff-sko-2022) (GitLab internal-only).

# SKO 2022 Day 1 Sessions

| **Session Name** | **Video** |
| ------ | ------ |
| Welcome Keynote | [video link](https://gitlab.edcast.com/insights/welcome-keynote) |
| Product Keynote | [video link](https://gitlab.edcast.com/insights/product-keynote) |
| Field Spotlight #1 | [video link](https://gitlab.edcast.com/insights/field-spotlight) |
| Customer Speaker | [video link](https://gitlab.edcast.com/insights/customer-speaker) |
| Field Spotlight #2 | [video link](https://gitlab.edcast.com/insights/field-spotlight-phreesia) |

# SKO 2022 Day 2 Sessions

| **Session Name** | **Video** |
| ------ | ------ |
| Motivational Speaker | [video link](https://gitlab.edcast.com/insights/motivational) |
| Channel & Alliances Keynote | [video link](https://gitlab.edcast.com/insights/channels) |
| Field Spotlight #3 | [video link](https://gitlab.edcast.com/insights/field-spotlight-conundra) |
| Marketing Keynote | [video link](https://gitlab.edcast.com/insights/marketing-keynote) |
| Field Spotlight #4 | [video link](https://gitlab.edcast.com/insights/field-spotlight-a) |
| Closing Keynote | [video link](https://gitlab.edcast.com/insights/closing-keynote) |

# SKO Awards Ceremony

| **Session Name** | **Video** |
| ------ | ------ |
| Awards Ceremony | [video link](https://gitlab.edcast.com/insights/sko-2022) |

# SKO 2022 Role-Based Breakout Sessions

## Enterprise Sales Strategic Account Leaders

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Delivering Strategic Customer Outcomes by Partnering with Your SAs | slides | video |
| Using Effective Proposals to Advance and Win Deals | slides | video |
| Supersonic pipeline: Best practices for prospecting to 4x | slides | video |

## Commercial Sales Account Executives

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| The Big Pitch | slides | video |
| Finding the "Why Now" | slides | video |
| The Journey to Ultimate, Part Deux | slides | video |

## Channel Sales Account Managers

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Developing your Select Partners to their Maximum Potential and Value Opportunity | slides | video |
| Building Pipeline with your Partners MDF and Marketing Campaigns | slides | video |
| Partnering with Alliances to Increase Revenue and Value | slides | video |

## Solution Architects

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Positioning the GitLab Agent for Kubernetes in your sales deal | slides | video |
| Value Stream Mapping - Origins, Assessments and Iterations | slides | video |
| GET Workshop | slides | video |

## Technical Account Managers

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| TAM Strategy | slides | video |
| TAM and Customer Personas | slides | video |
| Driving Adoption With CI/CD Workshops | slides | video |

## Professional Services Engineers

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Professional Services & Product: Working Better Together  | slides | video |
| How to use the GitLab Pipeline COE w/ Governance Pipelines | slides | video |

## Sales Development Reps

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Competitive Workshop | slides | video |
| Partnering with Sales: Good to Great | slides | video |
| Alumni Stories: Lessons Learned | slides | video |

## Field Marketing

| **Session Name** | **Presentation** | **Video** |
| ------ | ------ | ------ |
| Integrated Marketing Goals | slides | video |

